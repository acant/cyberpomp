# cyberpomp

Communications tool which stands between you and the internet.

# Similar Projects

* https://github.com/WorldBrain/Memex
* [Bloomberg Terminal](https://en.wikipedia.org/wiki/Bloomberg_Terminal)

# Inspiration

## Books

* [FreedomTM](https://en.wikipedia.org/wiki/Freedom%E2%84%A2), [https://en.wikipedia.org/wiki/Daniel_Suarez_(author)]
* Infomocracy, [Malka Older](https://en.wikipedia.org/wiki/Malka_Older)
* Stealing Worlds, [Karl Schroeder](https://en.wikipedia.org/wiki/Karl_Schroeder)
