Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'

  resource :inbox
  resource :feed
  resource :spacetime
  resources :entities
  resources :isbn
  resources :http
end
